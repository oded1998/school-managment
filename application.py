from flask import Flask
from flask_restful import Api
from flask_restful_swagger import swagger
from wsgiserver import WSGIServer
from SchoolManager.rest_api.rest_app import School, Class
from SchoolManager.utils.useful import read_json
from SchoolManager.conf.static_conf import DB_CONNECTION_RELATIVE_PATH
from SchoolManager.dal.connector import DbConnector
from SchoolManager.bl.data_fetcher import DataFetcher


def data_fetcher_init():
    env_conf = read_json(DB_CONNECTION_RELATIVE_PATH)
    conn = DbConnector(**env_conf)
    data_fetcher = DataFetcher(conn)
    return data_fetcher


data_fetcher = data_fetcher_init()

# for swagger ui go to /api.html
application = app = Flask(__name__)
api = swagger.docs(Api(app), api_spec_url='/api')
api.add_resource(School, '/school', resource_class_kwargs={'data_fetcher': data_fetcher})
api.add_resource(Class, '/class', resource_class_kwargs={'data_fetcher': data_fetcher})

if __name__ == '__main__':
    # server = WSGIServer(app, host="0.0.0.0", port=80)
    # server.start()
    app.run(debug=True)
