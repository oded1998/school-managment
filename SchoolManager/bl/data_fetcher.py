from sqlalchemy import select, insert

from SchoolManager.utils.useful import convert_orm_to_json


class DataFetcher(object):
    def __init__(self, db_connector):
        self.__db_connector = db_connector

    def get_schools(self):
        base = self.__db_connector.get_base()
        query = select([base.classes.School])
        with self.__db_connector as conn:
            schools_orm = conn.execute(query)
        schools_json = convert_orm_to_json(schools_orm)
        return schools_json

    def add_school(self, **kwargs):
        base = self.__db_connector.get_base()
        new_school = base.classes.School(SchoolName=kwargs['SchoolName'], SchoolCountry=kwargs['SchoolCountry'],
                                         SchoolAddress=kwargs['SchoolAddress']
                                         )

        query = new_school
        with self.__db_connector as conn:
            conn.execute(query, returnable=False)

    def get_classes(self):
        base = self.__db_connector.get_base()
        query = select([base.classes.Class])
        with self.__db_connector as conn:
            classes_orm = conn.execute(query)
        classess_json = convert_orm_to_json(classes_orm)
        return classess_json

    def get_class(self, class_id):
        base = self.__db_connector.get_base()
        query = select([base.classes.Class]).where(base.classes.Class.ClassId == class_id)
        with self.__db_connector as conn:
            classe_orm = conn.execute(query)
        class_json = convert_orm_to_json(classe_orm)
        return class_json

# hdf.add_school(**{'SchoolName':'a', 'SchoolAddress':'b', 'SchoolCountry':'c'})
