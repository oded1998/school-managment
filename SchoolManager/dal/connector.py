from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine
from base64 import b64decode


class DbConnector(object):
    def __init__(self, **kwargs):
        self.__hostname = kwargs['hostname']
        self.__username = kwargs['username']
        self.__password = b64decode(kwargs['password']).decode("utf-8", "ignore")
        self.__port = kwargs['port']
        self.__schema = kwargs['schema']
        self.__session = None
    def __get_engine(self):
        engine = create_engine(
            "mysql+pymysql://{user}:{password}@{hostname}:{port}/{schema}".format(user=self.__username,
                                                                                  password=self.__password,
                                                                                  hostname=self.__hostname,
                                                                                  port=self.__port,
                                                                                  schema=self.__schema))
        return engine

    def __enter__(self):
        engine = self.__get_engine()
        self.__session = Session(engine)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.__session.close()

    def get_base(self):
        engine = self.__get_engine()
        base = automap_base()
        base.prepare(engine, reflect=True)
        return base

    def execute(self, query, returnable=True):
        if returnable:
            resultset = self.__session.execute(query)
            res = resultset.fetchall()
            return res
        else:
            self.__session.add(query)
            self.__session.commit()

