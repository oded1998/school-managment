from flask import request
from flask_restful import Resource, reqparse
from flask_restful.representations import json
from flask_restful_swagger import swagger


class School(Resource):
    def __init__(self, data_fetcher):
        self.__data_fetcher = data_fetcher

    @swagger.operation()
    def get(self):
        try:
            schools_json = self.__data_fetcher.get_schools()
            return schools_json, 200
        except Exception as e:
            return e, 400

    @swagger.operation(
        parameters=[
            {
                "name": "SchoolName",
                "required": "True",
                "description": "the school name",
                "allowMultiple": "False",
                "dataType": "string",
                "paramType": "query"
            },
            {
                "name": "SchoolCountry",
                "required": "True",
                "description": "the country the school is located on",
                "allowMultiple": "False",
                "dataType": "string",
                "paramType": "query"
            },
            {
                "name": "SchoolAddress",
                "required": "True",
                "description": "the address of the school",
                "allowMultiple": "False",
                "dataType": "string",
                "paramType": "query"
            }
        ]
    )
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('SchoolName', type=str, help='the school name')
        parser.add_argument('SchoolCountry', type=str, help='the country the school is located on')
        parser.add_argument('SchoolAddress', type=str, help='the address of the school')
        args = parser.parse_args()
        try:
            self.__data_fetcher.add_school(**args)
            return json.dumps('successfully created school'), 201
        except Exception as e:
            return json.dumps(e), 400


class Class(Resource):
    def __init__(self, data_fetcher):
        self.__data_fetcher = data_fetcher

    @swagger.operation(parameters=[{
        "name": "class_id",
        "in": "query",
        "required": "True",
        "description": "the class id",
        "allowMultiple": "False",
        "dataType": "int",
        "paramType": "query"
    }])
    def get(self):
        class_id = request.args['class_id']
        try:
            return self.__data_fetcher.get_class(int(class_id))
        except Exception as e:
            return json.dumps(e), 400
