import argparse
import json
import logging
import os


# def argparser():
#     parser = argparse.ArgumentParser()
#     parser.add_argument("-e", "--environment", help="enter the environment the script runs at")
#     args = parser.parse_args()
#     return args
#
#
# def get_args(arg):
#     args = vars(argparser())
#     return args[arg]


# def get_logger_init(logger_name):
#     logger = logging.getLogger(logger_name)
#     logger.setLevel(logging.INFO)
#     ch = logging.StreamHandler()
#     ch.setLevel(logging.DEBUG)
#     formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
#     ch.setFormatter(formatter)
#     logger.addHandler(ch)
#     return logger


def read_json(file_relative_path):
    absolute_path = os.path.normpath(os.path.abspath(".") + file_relative_path)
    with open(absolute_path, 'r') as f:
        data = json.load(f)
    return data


def convert_orm_to_json(orms):
    orm_dict = dict()
    orm_dict["values"] = [dict(row.items()) for row in orms]
    return orm_dict
